Source: vdr
Section: video
Priority: optional
Maintainer: Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>
Uploaders: Tobias Grimm <etobi@debian.org>
Build-Depends: debhelper-compat (= 13), dh-exec (>= 0.3), bash-completion, libjpeg-dev, libcap-dev,
  libncursesw5-dev, libfreetype6-dev, libfontconfig-dev, gettext,
  linux-libc-dev (>= 3.0), libfribidi-dev, libsystemd-dev
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/vdr-team/vdr.git
Vcs-Browser: https://salsa.debian.org/vdr-team/vdr
Homepage: http://www.tvdr.de/

Package: vdr
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, adduser, lsb-base (>= 3.0-6)
Recommends: lirc, fonts-dejavu-core | ttf-bitstream-vera
Suggests: vdr-plugin-dvbsddevice
Provides: ${vdr:Provides}
Description: Video Disk Recorder for DVB cards
 Video Disk Recorder (VDR) is a digital sat-receiver program using
 Linux and DVB technologies. It allows one to record MPEG2 streams,
 as well as output the stream to TV. It is also possible to watch DVDs
 (hardware accelerated) with some comfort and use an IR remote control.
 .
 This package contains the VDR main program which is controlled via the
 PC keyboard or a IR remote control.
 .
 NOTE: You should install compatible DVB drivers before using these
 programs. Also the standard VDR (without special plugins) requires
 a DVB-Card with an integrated mpeg-decoder, a so called
 Full-Featured Card.

Package: vdr-dev
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, debhelper, linux-libc-dev (>= 3.0)
Suggests: dh-make, make
Description: Video Disk Recorder plugin development files
 Video Disk Recorder (VDR) is a digital sat-receiver program using
 Linux and DVB technologies. It allows one to record MPEG2 streams,
 as well as output the stream to TV. It is also possible to watch DVDs
 (hardware accelerated) with some comfort and use an IR remote control.
 .
 This package contains the header files of VDR.
 You need this package to be able to build vdr-plugins!

Package: vdr-plugin-examples
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, vdr (= ${binary:Version})
Replaces: vdr (<< 2.0.1-1)
Breaks: vdr (<< 2.0.1-1)
Enhances: vdr
Description: Plugins for vdr to show some possible features
 Video Disk Recorder (VDR) is a digital sat-receiver program using
 Linux and DVB technologies. It allows one to record MPEG2 streams,
 as well as output the stream to TV.
 .
 This package contains the example-plugins hello, osddemo, svccli,
 svcsvr, skincurses, status and svdrpdemo from the vdr-source.
 These plugins do not have useful features, they only demonstrate
 how vdr-plugins work and what is possible to do with them.
